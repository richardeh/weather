"""
Weather.py
Retrieves and displays the weather for a given location

Author: Richard Harrington
Date Created: 10/22/2013
Last Updated: 10/24/2013
Weather info from http://api.openweathermap.org/
"""

import json
from http.client import HTTPConnection
from math import floor

def get_weather(city,operation=""):
    url='api.openweathermap.org'
    req='/data/2.5/'+operation+'?q='+city+'&cnt=3'
    conn=HTTPConnection(host=url)
    try:
        conn.request('GET',req)
    except:
        print(Exception)

    content=conn.getresponse().read()
    json=content.decode(encoding='utf-8')

    return json

def k_to_f(temp):
    return floor((temp-273.15)*1.8+32)

city=input("Please enter a city in the format (city,state or country)")
data=json.loads(get_weather(city,"weather"))

print("Weather report for :"+city)
print("Temperature: "+str(k_to_f(data['main']['temp']))+"\xB0 F")
print("Weather conditions: "+data['weather'][0]['description'])
print("Humidity: "+str(data['main']['humidity']))
print("Today's high: "+str(k_to_f(data['main']['temp_max']))+"\xB0 F")

fore=json.loads(get_weather(city,"forecast/daily"))
print("\nForecast for tomorrow:")
print("Weather conditions: "+fore['list'][0]['weather'][0]['description'])
print("Temperatures: ")
print("Low: "+str(k_to_f(fore['list'][0]['temp']['min']))+"\xB0 F"+ \
            " High: "+str(k_to_f(fore['list'][0]['temp']['max']))+"\xB0 F")
